package com.atlassian.confluence.plugins.quicknav.tutorial.search;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.confluence.search.actions.json.ContentNameMatch;
import com.atlassian.confluence.search.lucene.DocumentFieldName;
import com.atlassian.confluence.search.v2.SearchFieldNames;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.confluence.util.actions.DisplayMapper;
import com.atlassian.core.filters.ServletContextThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

import static com.atlassian.confluence.util.HtmlUtil.htmlEncode;
import static java.util.Objects.requireNonNull;

@Component
class SearchResultTransformer {
    private final DisplayMapper displayMapper;
    private final UserAccessor userAccessor;

    @Autowired
    public SearchResultTransformer(@ComponentImport DisplayMapper displayMapper,
                                   @ComponentImport UserAccessor userAccessor) {
        this.displayMapper = requireNonNull(displayMapper);
        this.userAccessor = requireNonNull(userAccessor);
    }

    ContentNameMatch transform(SearchResult searchResult) {
        String className = displayMapper.getClassName(searchResult);

        String spaceName = searchResult.getSpaceName();
        if (!StringUtils.isEmpty(spaceName)) {
            spaceName = htmlEncode(spaceName);
        }

        String spaceKey = searchResult.getSpaceKey();
        if (!StringUtils.isEmpty(spaceKey)) {
            spaceKey = htmlEncode(spaceKey);
        }

        HttpServletRequest servletRequest = ServletContextThreadLocal.getRequest();

        ContentNameMatch contentNameMatch = new ContentNameMatch();

        contentNameMatch.setId(String.valueOf(getContentId(searchResult.getField(SearchFieldNames.HANDLE))));
        contentNameMatch.setClassName(className);

        String urlPath = searchResult.getField(SearchFieldNames.URL_PATH);
        contentNameMatch.setHref(servletRequest.getContextPath() + urlPath);

        if (PersonalInformation.CONTENT_TYPE.equals(searchResult.getType())) {
            String username = searchResult.getField(DocumentFieldName.USER_NAME);
            String iconUrl = getIconUriReferenceForUsername(username);
            contentNameMatch.setUsername(username);
            contentNameMatch.setIcon(iconUrl);
        }

        String contentName = searchResult.getField(SearchFieldNames.CONTENT_NAME_UNSTEMMED_FIELD);
        contentNameMatch.setName(htmlEncode(contentName));

        contentNameMatch.setSpaceName(spaceName);
        contentNameMatch.setSpaceKey(spaceKey);

        return contentNameMatch;
    }

    private static long getContentId(String handle) {
        try {
            return new HibernateHandle(handle).getId();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private String getIconUriReferenceForUsername(String username) {
        ConfluenceUser user = userAccessor.getUserByName(username);

        if (user == null) {
            return ServletContextThreadLocal.getRequest().getContextPath() + ProfilePictureInfo.ADGS_DEFAULT_PROFILE_PATH;
        } else {
            return userAccessor.getUserProfilePicture(user).getUriReference();
        }
    }
}
